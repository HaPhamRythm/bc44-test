import { QuanLySVReducer } from "./QuanLySVReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers(QuanLySVReducer);
