import { shoeArr } from "../../../V_Cart27_Redux/data";
import {
  ADD_TO_CART,
  DELETE_FROM_CART,
  INCREASE_DECREASE,
} from "../constant/constantCart";

let initialState = {
  cart: [],
};

export const cartReducer = (
  state = initialState,
  { type, payload, index, inDe }
) => {
  switch (type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => item.id === payload.id);
      if (index == -1) {
        cloneCart.push(payload);
        state.cart = cloneCart;
        return { ...state };
      } else {
        cloneCart[index].number++;
        state.cart = cloneCart;
        return { ...state };
      }
    }
    case DELETE_FROM_CART: {
      let cloneCart = [...state.cart];
      cloneCart.splice(payload, 1);
      state.cart = cloneCart;
      return { ...state };
    }
    case INCREASE_DECREASE: {
      let cloneCart = [...state.cart];
      if (inDe === 1) {
        cloneCart[index].number++;
      } else {
        if (cloneCart[index].number > 0) {
          cloneCart[index].number = cloneCart[index].number - 1;
        }
      }
      state.cart = cloneCart;
      return { ...state };
    }
    default:
      return state;
  }
};
