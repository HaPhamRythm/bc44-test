import React, { Component } from "react";
import CartModel from "./CartModel";
import ListShoes from "./ListShoes";

export default class Re_V_Cart27_Redux extends Component {
  render() {
    return (
      <div>
        <h1>Re_V_Cart27_Redux</h1>
        <div className="row">
          <CartModel />
          <ListShoes />
        </div>
      </div>
    );
  }
}
