import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderList = () => {
    return this.props.listToRender.map((item, index) => {
      return (
        <ItemShoe
          key={index}
          data={item}
          handleCLickToItemShoe={this.props.handleClickToListShoe}
          handleBuyToItemShoe={this.props.handleBuyToListShoe}
        />
      );
    });
  };
  render() {
    return <div className="row col-6">{this.renderList()}</div>;
  }
}
//{}
