import React, { Component } from "react";
import DanhSachSanPhamRedux from "./DanhSachSanPhamRedux";
import ModelGioHangRedux from "./ModelGioHangRedux";

export default class V_Cart27_Redux extends Component {
  render() {
    return (
      <div>
        <h1>V_Cart27_Redux</h1>
        <div className="row">
          <ModelGioHangRedux />
          <DanhSachSanPhamRedux />
        </div>
      </div>
    );
  }
}
