import React, { Component } from "react";
import { connect } from "react-redux";
import {
  CHANGE_QUANTITY,
  REMOVE_FROM_CART,
} from "./redux/constant/cartConstant";

class ModelGioHangRedux extends Component {
  renderCart = () => {
    return this.props.cartList.map((item, index) => {
      return (
        <tr key={index}>
          <td>
            <img src={item.image} width={100} height={100} alt="" />
          </td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={this.props.handleChangeQuantity(index, -1)}
              className="btn btn-warning"
            >
              -
            </button>
            {item.number}
            <button
              onClick={this.props.handleChangeQuantity(index, 1)}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>{item.number * item.price}</td>
          <button
            onClick={() => {
              this.props.handleDelete(item);
            }}
            className="btn btn-danger"
          >
            X
          </button>
        </tr>
      );
    });
  };

  render() {
    return (
      <div className="col-6">
        <h2>Shopping Cart</h2>
        <table className="table">
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Number</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return { cartList: state.cartReducer.cart };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (shoe) => {
      dispatch({
        type: REMOVE_FROM_CART,
        payload: shoe,
      });
    },
    handleChangeQuantity: (index, inDe) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { index, inDe },
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ModelGioHangRedux);
