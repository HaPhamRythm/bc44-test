import React, { Component } from "react";
import { shoeArr } from "./data";
import SanPhamRedux from "./SanPhamRedux";

export default class DanhSachSanPhamRedux extends Component {
  renderShoes = () => {
    return shoeArr.map((item, index) => {
      return <SanPhamRedux key={index} shoe={item} />;
    });
  };
  render() {
    return (
      <div className="col-6">
        <h2>List of Shoes</h2>
        <div className="row">{this.renderShoes()}</div>
      </div>
    );
  }
}
