import React, { Component } from "react";

export default class ProductList extends Component {
  render() {
    let { productArray, handleDelete, handleEdit1 } = this.props;

    return (
      <div>
        <h3>List of products</h3>
        <table class="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Image</th>
              <th>Name</th>
              <th>Price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {productArray.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>
                    <img src={item.image} alt="" />
                  </td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleDelete(item);
                      }}
                      className="btn btn-danger"
                    >
                      X
                    </button>
                    <button
                      onClick={() => {
                        handleEdit1(item.id);
                      }}
                      className="btn btn-primary"
                    >
                      Edit
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
