import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE, EDIT1 } from "./redux/constant/constantStudents";

class ProductList extends Component {
  render() {
    let { studentArray, handleDelete, handleEdit1 } = this.props;
    // let { id, name, phone, email } = student;
    return (
      <div className="container">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Phone Number</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {studentArray.map((item) => {
              return (
                <tr key={item.id}>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.phone}</td>
                  <td>{item.email}</td>
                  <td>
                    <button
                      onClick={() => {
                        handleDelete(item.id);
                      }}
                      className="btn btn-success"
                    >
                      X
                    </button>
                    <button
                      onClick={() => {
                        handleEdit1(item);
                      }}
                      className="btn btn-primary"
                    >
                      Edit
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { studentArray: state.studentsReducer.studentArray };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (idProduct) => {
      dispatch({
        type: DELETE,
        payload: idProduct,
      });
    },

    handleEdit1: (student) => {
      dispatch({
        type: EDIT1,
        payload: student,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
