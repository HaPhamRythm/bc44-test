import axios from "axios";
import React, { Component } from "react";

export default class Re_ShoeShopLifeCycleAxios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
  }
  getListFromAxios() {
    axios
      .get("https://shop.cyberlearn.vn/api/Product")
      .then((result) => {
        console.log(result);
        this.setState({ list: result.data.content });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  render() {
    return (
      <div className="container">
        <h1>ShoeShop with LifeCycle and Axios</h1>
        <div className="row">
          {this.state.list.map((item) => {
            return (
              <div className="col-4 mb-3" key={item.id}>
                <div className="card text-left">
                  <img className="card-img-top" src={item.image} alt />
                  <div className="card-body">
                    <h4 className="card-title">{item.alias}</h4>
                    <p className="card-text">{item.price}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.getListFromAxios();
  }
}
