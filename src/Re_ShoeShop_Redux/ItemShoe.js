import React, { Component } from "react";
import { connect } from "react-redux";
import { BUY, VIEW_DETAIL } from "./redux/constant/shoeConstant";
import { viewDetailAction } from "./redux/action/shoeAction";

class ItemShoe extends Component {
  render() {
    let { data, handleCLickToItemShoe, handleBuyToItemShoe } = this.props;
    let { image, name, price } = data;
    return (
      <div className="col-6 mb-3">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h4 className="card-title">{name}</h4>
            <p class="card-text">Price: {price}</p>
          </div>
          <div>
            <button
              className="btn btn-primary mx-5"
              onClick={() => {
                handleCLickToItemShoe(data);
              }}
            >
              See More ...
            </button>
            <button
              onClick={() => {
                handleBuyToItemShoe(data);
              }}
              className="btn btn-success"
            >
              Buy
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleCLickToItemShoe: (shoe) => {
      dispatch(viewDetailAction(shoe));
    },

    handleBuyToItemShoe: (shoe) => {
      dispatch({
        type: BUY,
        payload: shoe,
      });
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
