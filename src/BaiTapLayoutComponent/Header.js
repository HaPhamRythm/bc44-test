import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div>
        <nav class="navbar navbar-expand-lg bg bg-dark ">
          <div class="container">
            <a class="navbar-brand text-white" href="#">
              Start Bootstrap
            </a>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link text-white active" href="#">
                    Home
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link text-white" href="#">
                    About
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link text-white" href="#">
                    Services
                  </a>
                </li>

                <li class="nav-item">
                  <a class="nav-link text-white" href="#">
                    Contact
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
