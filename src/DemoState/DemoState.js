import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    quantity: 1,
  };
  handleIncrease = () => {
    this.setState({
      quantity: this.state.quantity + 1,
    });
  };
  handleDecrease = () => {
    this.setState({
      quantity: this.state.quantity - 1,
    });
  };
  render() {
    return (
      <div>
        <h2
          style={{
            color: "red",
          }}
        >
          DemoState
        </h2>
        <button onClick={this.handleDecrease}>-</button>
        <span>{this.state.quantity}</span>
        <button onClick={this.handleIncrease}>+</button>
      </div>
    );
  }
}
